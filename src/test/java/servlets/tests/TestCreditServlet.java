package servlets.tests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import servlets.CreditServlet;

public class TestCreditServlet extends Mockito{
	@Test
	public void servlet_should_redirect_if_credit_zero_or_below() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		CreditServlet servlet = new CreditServlet();
		//when(request.getParameter("name")).
//        when(request.getParameter("amount")).thenReturn("0", "-1");
        
        when(request.getParameter("installments")).thenReturn("3", "3");
        when(request.getParameter("percentage")).thenReturn("0.1","0.1");
        when(request.getParameter("fixedFee")).thenReturn("0", "0");
        when(request.getParameter("fixedRates")).thenReturn("true", "false");
        
        when(request.getParameter("amount")).thenReturn("-1", "1");

        servlet.doGet(request, response);
        verify(response).sendRedirect("/");
        
        servlet.doGet(request, response);
        verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_redirect_if_installments_zero_or_below() throws IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		CreditServlet servlet = new CreditServlet();
        
        when(request.getParameter("percentage")).thenReturn("0.1","0.1");
        when(request.getParameter("fixedFee")).thenReturn("0", "0");
        when(request.getParameter("fixedRates")).thenReturn("true", "false");
        when(request.getParameter("amount")).thenReturn("100", "99999");

        when(request.getParameter("installments")).thenReturn("0", "-1");
        
        servlet.doGet(request, response);
        verify(response).sendRedirect("/");
        
	}
	
	@Test
	public void servlet_should_redirect_if_fixedFee_below_zero() throws IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		CreditServlet servlet = new CreditServlet();
        
        when(request.getParameter("percentage")).thenReturn("0.1");
        when(request.getParameter("fixedRates")).thenReturn("true");
        when(request.getParameter("amount")).thenReturn("100");
        when(request.getParameter("installments")).thenReturn("0");
        
        when(request.getParameter("fixedFee")).thenReturn("-1");
        
        servlet.doGet(request, response);
        verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_redirect_if_percentage_below_zero() throws IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		CreditServlet servlet = new CreditServlet();
        
        when(request.getParameter("fixedFee")).thenReturn("0", "0");
        when(request.getParameter("fixedRates")).thenReturn("true", "false");
        when(request.getParameter("amount")).thenReturn("100", "99999");
        when(request.getParameter("installments")).thenReturn("0", "-1");

        when(request.getParameter("percentage")).thenReturn("-0.1");
        
        servlet.doGet(request, response);
        verify(response).sendRedirect("/");
	}
}
