package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import credit.Credit;
import credit.CreditInstallment;

@WebServlet("/credit")
public class CreditServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		int amount;
		int installments;
		float percentage;
		int fixedFee;
		boolean fixedRates;
		
		try{
			amount = Integer.parseInt(request.getParameter("amount"));
			installments = Integer.parseInt(request.getParameter("installments"));
			percentage = Float.parseFloat(request.getParameter("percentage"));
			percentage = percentage / 100f;
			fixedFee = Integer.parseInt(request.getParameter("fixedFee"));
			fixedRates = Boolean.parseBoolean(request.getParameter("fixedRates"));
		}catch (NumberFormatException e){
			response.sendRedirect("/");
			return;
		}
		if (amount <= 0 || installments <= 0 || percentage < 0 || fixedFee < 0){
			response.sendRedirect("/");
			return;
		}
		
			response.setContentType("text/html");
			response.getWriter().println("<table>");
			response.getWriter().println("<tr><td>Nr raty</td><td>Calkowita Rata</td><td>Kapital</td><td>Kwota odsetek</td><td>Kwota kapitalu</td> <td>Oplaty stale</td></tr>");
			Credit credit = new Credit();
			credit.setAmount(amount);
			credit.setFixedFee(fixedFee);
			credit.setInstallments(installments);
			credit.setPercentage(percentage);
			credit.setFixedRates(fixedRates);
			List<CreditInstallment> installmentsList = credit.getInstallmentsList();
			for(int i=0; i<installmentsList.size(); i++){
				CreditInstallment c = installmentsList.get(i);
				response.getWriter().println("<tr><td>" + Integer.toString(i+1) +"</td><td>" + Double.toString(c.getFullInstallment()) + "</td><td> " + Double.toString(c.getRestOfCapital()) + " </td><td>" + Double.toString(c.getInterest())
				+ "</td> <td>" + Double.toString(c.getCapitalRate()) + "</td><td>" + Double.toString(c.getFixedFee()) + "</td></tr>");
				
			}
		
	}
}
