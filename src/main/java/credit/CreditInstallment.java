package credit;

public class CreditInstallment {
	private double fullInstallment;
	private double capitalRate;
	private double restOfCapital;
	private double interest;
	private double fixedFee;
	public CreditInstallment(double rata, double rataKapital, double kapital, double odsetki, double oplata){
		this.fullInstallment = rata;
		this.capitalRate = rataKapital;
		this.restOfCapital = kapital;
		this.interest = odsetki;
		this.fixedFee = oplata;
	}
	public double getFullInstallment() {
		return fullInstallment;
	}
	public double getCapitalRate() {
		return capitalRate;
	}
	public double getRestOfCapital() {
		return restOfCapital;
	}
	public double getInterest() {
		return interest;
	}
	public double getFixedFee() {
		return fixedFee;
	}
	
	
}
