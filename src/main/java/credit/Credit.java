package credit;

import java.util.ArrayList;
import java.util.List;

public class Credit {
	private float amount = 100;
	private int installments = 1;
	private float fixedFee = 0;
	private boolean fixedRates = false;
	private float percentage = 0;
	
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}

	private List<CreditInstallment> installmentsList = new ArrayList<CreditInstallment>(); 
	public Credit(){
		
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public int getInstallments() {
		return installments;
	}
	public void setInstallments(int installments) {
		this.installments = installments;
	}
	public float getFixedFee() {
		return fixedFee;
	}
	public void setFixedFee(float fixedFee) {
		this.fixedFee = fixedFee;
	}
	public boolean isFixedRates() {
		return fixedRates;
	}
	public void setFixedRates(boolean fixedRates) {
		this.fixedRates = fixedRates;
	}
	
	public List<CreditInstallment> getInstallmentsList(){
		installmentsList.clear();
		if (fixedRates){
			int n = installments;
			double q = 1 + percentage/installments;
			double rata = amount * Math.pow(q, n) * (q-1)/(Math.pow(q, n) - 1);
			double kapital = amount;

			for (int i=0; i<n; i++){
				double odsetki = percentage/installments * kapital;
				double rataKapital = rata - odsetki;
				double oplata = fixedFee/installments;
				
				CreditInstallment installment = new CreditInstallment(rata + oplata, rataKapital, kapital, odsetki, oplata);
				installmentsList.add(installment);
				
				kapital = kapital - rataKapital;
			}
			
		}else{
			double kapital = amount;
			for (int i=0; i<installments; i++){
				int n = i + 1;
				double rata = amount/installments * ( 1 + (installments - n + 1) * percentage/installments);
				double rataKapital = amount/installments;
				double rataOdsetki = rata - rataKapital;
				
				double oplata = fixedFee/installments;
				CreditInstallment installment = new CreditInstallment(rata + oplata, rataKapital, kapital, rataOdsetki, oplata);
				installmentsList.add(installment);
		
				kapital -= rataKapital;
			}
		}
		return installmentsList;
	}

}
